# Welcome, the entire land! [1]

Hi, all. I'm a web nerd from Portugal 🇵🇹. You can find me across the web under `andr3`, usually.

[1] if you're wondering about the title, that's the closest you can write `Hello, world!` using hieroglyphics. Cute story of it at https://optional.is/required/2009/12/03/welcome-the-entire-land/

# Interests

Throughout my years on this earth, I amassed a few interests. If you're into any of these, feel free to jump on my [calendly](https://calendly.com/andr3) and schedule a coffee chat. This goes to GitLab Team Members, Community Contributors, lurkers, humans, spambots... I must admit I'd be particularly interested in having a coffee chat with a spambot. 😂

Here are some of the topics I'm into:

* Frontend engineering: heavy CSS nerd and big on web standards
* Accessibility
* User Experience
* Photography: landscape, architecture
* Gaming / Streaming
* Movies: oldies, indies
* Typography & calligraphy
* Cryptocurrency / Web3
* Misinformation

# Working

I work as a Frontend Engineering Manager at GitLab in the groups **Create:Source Code** and **Create:Code Review**. I'm also the frontend counterpart for the **Delivery** and **Scalability** teams.

As a manager, my Direct Reports 1:1s are my absolute priority. On top of that, I do have a meeting-heavy agenda, which I love.

Please, if I'm taking too long getting back to you, feel free to ping me directly via slack, email or any other way you see fit. With a constant barrage of mentions and pings, ToDos are a bit out of hand... so it's not unusual for some things slip through the cracks. Apologies and thank you.

# GitLab

I've joined GitLab in February 2018 as a Senior Frontend Engineer and have been officially an Engineering Manager since Jan 2019. I've managed the frontend teams in Plan and Create, when one team covered the whole stage. After Create broke up into smaller groups, I managed Source Code, Knowledge and Editor. Then I focused on Source Code only, which eventually broke up to Source Code and Code Review, for which I still oversee the Frontend teams.

# Elsewhere

In 2014 I created this (severely outdated) website: http://meet.andr3.net And my old blog, which I wrote from scratch while in college circa 2004, is still up at http://andr3.net/blog/ Enjoy the nerdy youngster I was back then.

Around social media, I go by andr3 or a slight variation of that, if I arrived too late to secure the proper one.

* https://twitter.com/andr3
* https://www.linkedin.com/in/andr3/
* https://www.instagram.com/andr3/
* https://flickr.com/photos/andr3/
* https://www.twitch.tv/n3cr0_pt/
